import { createAction, props } from "@ngrx/store";
import { Bookmark } from "src/app/model/bookmark";

export const addBookmarkAction = createAction(
  'bookmark add request',
  props<Bookmark>()
)

export const deleteBookmarkAction = createAction(
  'bookmark delete request',
  props<{id: number}>()
)
