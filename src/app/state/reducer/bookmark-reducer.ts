import { createReducer, on } from "@ngrx/store";
import { Bookmark } from "src/app/model/bookmark";
import { addBookmarkAction, deleteBookmarkAction } from "../action/bookmark-action";

export interface BookmarkState {
  bookmarks: Bookmark[];
}

const initialState :BookmarkState = {
  bookmarks: [
    {id: 1, name: 'Hydrogen', url: 'www.google.com', group: 'work'},
    {id: 2, name: 'Hydrogen', url: 'www.faceboo.com', group: 'leisure'},
    {id: 3, name: 'Hydrogen', url: 'www.angular.io', group: 'personal'},
  ]
};

export const bookmarkReducer = createReducer(
  initialState,

  on(addBookmarkAction, (state, bookmark) => {
    // temporarily solution for incrementatl id
    const id = state.bookmarks.length ? state.bookmarks[state.bookmarks.length-1].id + 1 : 0;

    return {
      ...state,
      bookmarks: [...state.bookmarks, {...bookmark, id}]
    }
  }),

  on(deleteBookmarkAction, (state, props: {id: number}) => {
    const index = state.bookmarks.findIndex(el => el.id === props.id);
    const bookmarks = [...state.bookmarks];
    bookmarks.splice(index, 1);
    return {
      ...state,
      bookmarks: bookmarks
    }
  })
)
