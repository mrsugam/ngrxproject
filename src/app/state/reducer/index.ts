import { ActionReducerMap } from '@ngrx/store';
import { Bookmark } from "src/app/model/bookmark";
import { bookmarkReducer, BookmarkState } from './bookmark-reducer';

export interface RootReducerState {
  bookmarks: BookmarkState;
}

export const rootReducer : ActionReducerMap<RootReducerState> = {
  bookmarks: bookmarkReducer
}

export const bookmarksSelector = (rootState: RootReducerState): Bookmark[] => rootState.bookmarks.bookmarks;
