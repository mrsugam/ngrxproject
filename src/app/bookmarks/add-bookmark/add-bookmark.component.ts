import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { BookmarksService } from 'src/app/services/bookmarks.service';
import { addBookmarkAction } from 'src/app/state/action/bookmark-action';
import { RootReducerState } from 'src/app/state/reducer';

@Component({
  selector: 'app-add-bookmark',
  templateUrl: './add-bookmark.component.html',
  styleUrls: ['./add-bookmark.component.scss']
})
export class AddBookmarkComponent implements OnInit {

  public addBookmark: FormGroup;

  constructor(fb: FormBuilder, private bookmarkService: BookmarksService,
    private route: ActivatedRoute, private router: Router,private store: Store<RootReducerState>
    ) {
      const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.addBookmark = fb.group({
      name: ["", Validators.required],
      url: ["", [Validators.required, Validators.pattern(reg)]],
      group: ["", Validators.required]
  });
   }

  ngOnInit(): void {
  }

  addNewBookmark(){
      this.store.dispatch(addBookmarkAction(this.addBookmark.value));
     // this.bookmarkService.addNew(this.addBookmark.value).subscribe(res => {
        console.log("Added Successfully");
        this.router.navigate(['/bookmarks', this.route.snapshot.params]);
     // })
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.addBookmark.controls[controlName].hasError(errorName);
  }



}
