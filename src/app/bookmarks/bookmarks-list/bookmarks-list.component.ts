import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Bookmark } from 'src/app/model/bookmark';
import { BookmarksService } from 'src/app/services/bookmarks.service';
import { deleteBookmarkAction } from 'src/app/state/action/bookmark-action';
import { bookmarksSelector, RootReducerState } from 'src/app/state/reducer';

@Component({
  selector: 'app-bookmarks-list',
  templateUrl: './bookmarks-list.component.html',
  styleUrls: ['./bookmarks-list.component.scss']
})
export class BookmarksListComponent implements OnInit {



  displayedColumns: string[] = ['id', 'name', 'url', 'group','actions'];
  dataSource: MatTableDataSource<Bookmark> = new MatTableDataSource();

  constructor(private store: Store<RootReducerState>) {   }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const getBookmarkData = this.store.select(bookmarksSelector);

    getBookmarkData.subscribe(bookmarks => {
      this.dataSource.data = bookmarks;
      // console.log(bookmarks);
    });
  }

  deleteBookmark(id: number){
    this.store.dispatch(deleteBookmarkAction({id}));
  }

}
