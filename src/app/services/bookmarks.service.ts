import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Bookmark } from '../model/bookmark';

const ELEMENT_DATA: Bookmark[] = [
  {id: 1, name: 'Hydrogen', url: 'www.google.com', group: 'sreach'},
  {id: 2, name: 'Hydrogen', url: 'www.faceboo.com', group: 'social'},
  {id: 3, name: 'Hydrogen', url: 'www.angular.io', group: 'tutorial'},
];

@Injectable({
  providedIn: 'root'
})
export class BookmarksService {

  constructor(private httpService: HttpClient) { }



  getAll():Observable<Bookmark[]>{
    return of(ELEMENT_DATA);
  }

  addNew(bookmark: Bookmark):Observable<Bookmark>{
    const lastid = ELEMENT_DATA.reduce((maxId, el) => {
      return Math.max(maxId, el.id);
    }, 0);
    bookmark.id = lastid + 1;
    ELEMENT_DATA.push(bookmark);
    return of(bookmark);
  }

  remove(id: number):Observable<number>{
    const index = ELEMENT_DATA.findIndex(el => el.id === id);
    ELEMENT_DATA.splice(index, 1);
    return of(id);
  }

}
